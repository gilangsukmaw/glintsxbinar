const index = require("../Day 10/Event");

const array = [
  {
    name: "John",
    status: "Positive",
  },
  {
    name: "Mike",
    status: "Suspect",
  },
  {
    name: "Emily",
    status: "Negative",
  },
  {
    name: "Melissa",
    status: "Positive",
  },
  {
    name: "Angela",
    status: "Suspect",
  },
  {
    name: "Alvin",
    status: "Positive",
  },
  {
    name: "Oconnor",
    status: "Positive",
  },
  {
    name: "Craig",
    status: "Negative",
  },
];

function printNegative() {
  console.log(`===================`);
  console.log(`       Result`);
  console.log(`===================`);
  //find and filter if there is person with status "Negative"
  array.filter((item) => {
    item.status === "Negative"
      ? console.log(`${item.name}  is Negative`) // print all the person with status "Negative"
      : null;
  });

  console.log();
  console.log("Back to menu...");
  choose(); //back to menu
}

function printSuspect() {
  console.log(`===================`);
  console.log(`       Result`);
  console.log(`===================`);
  //find and filter if there is person with status "Suspect"
  array.filter((item) => {
    item.status === "Suspect" ? console.log(`${item.name}  is Suspect`) : null; // print all the person with status "Suspect"
  });

  console.log();
  console.log("Back to menu...");
  choose();
}

function printPositive() {
  console.log(`===================`);
  console.log(`       Result`);
  console.log(`===================`);
  //find and filter if there is person with status "Positive"
  array.filter((item) => {
    item.status === "Positive"
      ? console.log(`${item.name}  is Positive`) // print all the person with status "Positive"
      : null;
  });

  console.log();
  console.log("Back to menu...");
  choose();
}

function choose() {
  console.log();
  console.log(`       Menu`);
  console.log(`===================`);
  console.log(`1. Positive`);
  console.log(`2. Negative`);
  console.log(`3. Suspect`);
  console.log(`4. Exit`);
  index.rl.question(`Choose option: `, (option) => {
    switch (option) {
      case "1":
        printPositive();
        break;

      case "2":
        printNegative();
        break;

      case "3":
        printSuspect();
        break;

      case "4":
        index.rl.close();
        break;

      default:
        console.log("Sorry, option is not available");
    }
  });
}

module.exports = { choose };
