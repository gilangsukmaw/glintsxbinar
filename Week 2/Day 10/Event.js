const EventEmmiter = require("events");
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const e = new EventEmmiter();
const assignment = require("../Day 9/Assignment2");

e.on("login:success", function (username, password) {
  assignment.choose();
});

e.on("login:failed", function () {
  console.log(`===================`);
  console.log("Wrong credential, please try again!");
  console.log(`===================`);
});

function login(email, password) {
  const savedEmail = "qwerty123@gmail.com";
  const savedPassword = "mypassword";

  if (email !== savedEmail || password !== savedPassword) {
    e.emit("login:failed");
    menu();
  } else {
    e.emit("login:success");
  }
}

function menu() {
  console.log(`       Login`);
  console.log(`===================`);
  rl.question(`Enter your email : `, (email) => {
    rl.question(`Enter your password : `, (password) => {
      login(email, password);
    });
  });
}

menu();
module.exports.rl = rl; // export rl to make another can run the readline
