const index = require("../index"); // Import index to run index.rl on this file

function isEmptyOrSpaces(length) {
  return length === null || length.match(/^ *$/) !== null;
}

function CubeVolume(panjang, lebar, tinggi) {
  const formula = panjang * lebar * tinggi;
  return formula;
}

// Function for calculate Cube
function calculateCube() {
  console.log("+==============================================+");
  console.log("+                  Cube Volume                 +");
  console.log("+==============================================+");
  index.rl.question(`Enter the LENGTH of the Cube: `, (length) => {
    index.rl.question("Enter the WIDTH of the Cube: ", (width) => {
      index.rl.question("Enter the HEIGHT of the Cube: ", (height) => {
        if (!isNaN(length) && !isNaN(width) && !isNaN(height)) {
          if (index.isEmptyOrSpaces(length)) {
            console.log("Option must not have a space");
            calculateCube(); // If option is not 1 to 3, it will go back to the menu again
          }
          const value = CubeVolume(length, width, height);
          console.log("Cube volume is : ", value);
          index.rl.close();
        } else {
          console.log(`Length, Width and Height must be a number\n`);
          calculateCube();
        }
      });
    });
  });
}

module.exports = { calculateCube };
