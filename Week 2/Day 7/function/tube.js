const index = require("../index"); // Import index to run index.rl on this file

function TubeVolume(diameter, tinggi) {
  r = diameter / 2;
  const formula = Math.PI * r * r * tinggi;
  return formula;
}

// Function for calculate Tube
function calculateTube() {
  console.log("+==============================================+");
  console.log("+                   Tube Volume                +");
  console.log("+==============================================+");
  index.rl.question(`Enter the DIAMETER of the Tube: `, (diameter) => {
    index.rl.question("Enter the HEIGHT of the Tube: ", (height) => {
      if (!isNaN(diameter) && !isNaN(height)) {
        const value = TubeVolume(diameter, height);
        console.log("Tube volume is : ", value);
        index.rl.close();
      } else {
        console.log(`Diameter and Height must be a number\n`);
        calculateTube();
      }
    });
  });
}

module.exports = { calculateTube };
