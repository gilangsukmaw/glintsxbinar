const express = require("express");

const app = express();
const port = process.env.PORT || 5000;

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

//routes
const studentroutes = require("./routes/database");
// const example = require("./routes/example");

app.use("/student", studentroutes);
// app.use("/example", example);
app.listen(port, () => {
  console.log(`server running on port ${port}`);
});
