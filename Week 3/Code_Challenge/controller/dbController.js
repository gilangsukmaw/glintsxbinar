const students = require("../models/students.json");

class dbController {
  getAllStudents = async (req, res) => {
    try {
      res.status(200).json({
        data: students,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  };

  getSpecifyStudents = async (req, res) => {
    try {
      const idExist = await students.find((data) => data.id == req.params.id);
      if (!idExist) {
        res.status(200).json({
          message: "Student info not found!",
        });
      }
      const getData = await students.filter((data) => data.id == req.params.id);

      res.status(200).json(getData[0]);
    } catch (error) {
      res.status(500).json({
        message: "no data found",
      });
    }
  };

  addStudents = async (req, res) => {
    try {
      const idExist = await students.find((data) => data.id == req.body.id);
      if (idExist) {
        res.status(200).json({
          message: "Id already exist!",
        });
      } else {
        await students.push(req.body);
        res.status(200).json(students);
      }
    } catch (error) {
      res.status(500).json({
        message: "There an error",
      });
    }
  };

  editStudent = async (req, res) => {
    try {
      const idExist = await students.find((data) => data.id == req.params.id);

      if (idExist) {
        const update = await students.find((data) => {
          if (data.id == req.params.id) {
            data.name = req.body.name;
          }
        });
        res.status(200).json({
          message: "Student updated succesfully",
          data: idExist,
        });
      } else {
        res.status(200).json({
          message: "No data found",
        });
      }
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  };

  deleteStudent = async (req, res) => {
    try {
      const idExist = await students.find((data) => data.id == req.params.id);

      if (idExist) {
        const index = await students.indexOf(idExist);
        if (index > -1) {
          students.splice(index, 1);
        }
        res.status(200).json({
          message: "Sucessfully delete",
          data: students,
        });
      } else {
        res.status(200).json({
          message: "No data found",
        });
      }
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  };
}

module.exports = new dbController();
