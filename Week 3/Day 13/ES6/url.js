const url = [
  "https://project-exercise-tracker.herokuapp.com/exercise/", //get all exercise list
  "https://project-exercise-tracker.herokuapp.com/users/", //get all users
  "https://project-exercise-tracker.herokuapp.com/exercise/add", // You can add you own exercise
];

module.exports = url;
